---
layout: page
title: Events
permalink: /events/
---

<style>
	/*
	   Gets rid of default table coloring from Jenkins, which would
	   otherwise override the custom colors
	 */
	table tbody tr:nth-child(odd)
	td, table tbody tr:nth-child(odd) th {
	    background-color: transparent;
	}

	.presentation {
		background-color: #ffcccc;
	}

	.dinner {
		background-color: #ccccff;
	}

	.hack-day {
		background-color: #cccccc;
	}

	.day-column {
		width: 20%;
	}

	.type-column {
		width: 35%;
	}

	.location-column {
		width: 30%;
	}

	.time-column {
		width: 15%;
	}
</style>

For fall 2020, our meetings will alternate between presentations and online
"hang outs."

<table width="100%">
  <tr>
    <th class="type-column">Type</th>
    <th class="location-column">Location</th>
    <th class="time-column">Time</th>
  </tr>
  <tr class="presentation">
    <td>Presentation meeting</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>Dinner meeting</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
</table>

The specific meeting schedule is as follows:

<table width="100%">
  <tr>
    <th class="day-column">Date</th>
    <th class="type-column">Topic</th>
    <th class="location-column">Location</th>
    <th class="time-column">Time</th>
  </tr>
  <tr class="presentation">
    <td>Aug 11, 2020</td>
    <td>Test of Jitsi/planning</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>Aug 18, 2020</td>
    <td>Hang out/elections</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="presentation">
    <td>Aug 25, 2020</td>
    <td>Turning physical books into epubs with open source software</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>Sep 1, 2020</td>
    <td>Hang out</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="presentation">
    <td>Sep 8, 2020</td>
    <td>Teraform</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>Sep 15, 2020</td>
    <td>Hang out</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="presentation">
    <td>Sep 22, 2020</td>
    <td>High Performance Computing</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>Sep 29, 2020</td>
    <td>Hang out</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="presentation">
    <td>Oct 6, 2020</td>
    <td>Qubes</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>Oct 13, 2020</td>
    <td>Hang out</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="presentation">
    <td>Oct 20, 2020</td>
    <td>Networking (specifics TBD)</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>Oct 27, 2020</td>
    <td>Hang out</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="presentation">
    <td>Nov 3, 2020</td>
    <td>Lightning talks - Vim intro, containerization</td>
    <td><a href="https://meet.lug.ncsu.edu/meeting">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
</table>
