---
layout: page
title: Contact
permalink: /contact/
---

**Mailing List:**

    lug@lists.ncsu.edu

    To subscribe, email: lug+subscribe@lists.ncsu.edu

    Archives (as of 2020): https://groups.google.com/a/lists.ncsu.edu/g/lug
    Archives (2003-2019): Probably lost due to majordomo migration
    Archives (1994-2002): https://groups.google.com/g/ncsu.os.linux

**IRC:**

    Server: chat.freenode.net
    Port: 6667 | 6697 (SSL)
    Channel: #ncsulug

    Web client:
    http://webchat.freenode.net/?channels=ncsulug

**Postal mail:**

    Linux Users Group
    c/o SORC
    Campus Box 7295
    Raleigh, NC 27695
