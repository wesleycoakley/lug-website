---
layout: post
title:  Fall planning
---

Hey everyone,

If you're on the IRC this won't be news to you, but we're planning for fall
2020 (assuming that it will be like a normal semester). In order to help plan,
please help us out by doing the following:

1. Fill out your availability for meetings [here][when2meet] (please include a
   name/identifier to help us out).

1. If you have any ideas for a talk you'd like to give, please let us know!
   Whether it's a quick talk or a long presentation, we'd love to have you.

1. If you've given a presentation in the past and it isn't up on the website,
   please email it to me or add it to our [GitLab][gitlab]. It does not have
   to be a PDF - as long as it can be viewed without too much hassle it should
   be fine.

1. If you would like to be considered for an office role, please let me know.
   Most of these roles involve literally no extra work, so it's just a free
   resume booster. (You must be a student to hold an office role).

If the coming semester is canceled, I'd still like to try and do meetings in
some form. We could do them through zoom, though I know some would prefer to
avoid using it due to security risks. I've been using a program called
bluejeans a lot at work, and I've been impressed with how well it works with
Linux, so that's another possibility. If you have an alternate idea,
let us know!

POTLUG

[when2meet]: https://www.when2meet.com/?9112145-GfTfK
[gitlab]:    https://gitlab.com/ncsulug/lug-website
